package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();
    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
            return TauxTva.NORMAL.taux;
    }
    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux; 
        }
        catch ( Exception ex ) {
            throw new NiveauTvaInexistantException();
        }
    }

    @GET
    @Path("{niveauTva}")
    public double getMontantTotal(@QueryParam("somme") int somme, @PathParam("niveauTva") String niveauTva) {
    	try {
    		return CalculTva.calculerMontant(TauxTva.valueOf(niveauTva.toUpperCase()), somme);
	    }
	    catch ( Exception ex ) {
	        throw new NiveauTvaInexistantException();
	    }
    }
    @GET
    @Path("lestaux")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getInfoTaux() {
      ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
      for ( TauxTva t : TauxTva.values() ) {
        result.add(new InfoTauxDto(t.name(), t.taux));
      }
      return result;
    }
    
    @GET
    @Path("details/{niveauTva}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<InfoTauxDto> getDetail(@QueryParam("somme") int somme, @PathParam("niveauTva") String niveauTva){
    	ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
    	InfoTauxDto tauxTdo = new InfoTauxDto("montantTotal", CalculTva.calculerMontant(TauxTva.valueOf(niveauTva.toUpperCase()), somme));
    	result.add(tauxTdo);
    	result.add(new InfoTauxDto("montantTva", TauxTva.valueOf(niveauTva.toUpperCase()).taux));
    	result.add(new InfoTauxDto("somme", somme));
    	return result;
    }
}
